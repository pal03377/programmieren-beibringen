# Gegeben sei eine Zahl x. Wenn x eine Quadratzahl ist, berechne die Wurzel.
# Ansonsten ist es egal, was du machst.
x = 3721

# Dein Code...

# Für schnelle:
# Vermutlich gehst du gerade einfach viele Zahlen durch. 
# Das braucht halt leider relativ lang, weil du z.B. für 
# 10000^2 halt 10000 Schritte durchprobieren musst.
# Wie könnte das schneller gehen?
# (eine der Aufgaben für's Frühstudium bei mir :-) )