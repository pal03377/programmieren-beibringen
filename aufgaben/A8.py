# Teil 1: 
# Gegeben eine Zahl x in Radians, berechne 
# den Sinus von x und gib ihn aus.
x = 0.8

# Dein Code...

# Teil 2:
# Speichere sin(x) in einer Variable und 
# suche dann zwei Werte x, y zwischen 1 und 100, 
# sodass x/y ungefähr dem Wert von sin x entspricht, 
# bis auf 0,0001 genau. Es ergeben sich zwei Lösungen.
# (Tipp unten)

# Dein Code...













































# Tipp 1 für Teil 2:
# Benutze zwei innereinander verschachtelte for-Schleifen.
# (weiterer Tipp unten)







































# Tipp 2 für Teil 2:
# Du musst in den for-Schleifen vermutlich mit einem if unterscheiden, ob 
# x/y > sin(x) oder anders herum und dann dementsprechend die zweite Bedingung 
# für den Abstand anpassen.