# Collatz-Problem / 3n+1-Vermutung
# Es geht um eine Folge ausgehend von einer Zahl x.
# Ein Schritt in der Folge sieht dabei wie folgt aus:
# - wenn x gerade ist, wird der nächste Wert von x halbiert: x_neu = x / 2
# - wenn x ungerade ist, ist x_neu = 3 * x + 1
# Das macht man so lange, bis x = 1.
# Es ist nicht bekannt, ob immer irgendwann x = 1 erreicht wird. Es scheint aber so.
# Aufgabe: Schreibe ein Programm, das für ein gegebenes x diese Folge ausgibt.

x = 13 # Beispielzahl
# Dein Code...

# Die Folge hier wäre
# 40
# 20
# 10
# 5
# 16
# 8
# 4
# 2
# 1
# Vielleicht schreibt Python da noch jeweils ".0" dahinter, das macht nichts.