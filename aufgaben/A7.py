# Finde mithilfe zweier geschachtelter for-Schleifen 
# alle Primzahlen bis x:
# (Tipps unten)
x = 100 # oder so










































# Tipp 1: Die innere for-Schleife prüft für alle möglichen Zahlen, ob 
# sie die Zahl der äußeren for-Schleife teilt.





































# Tipp 2: Du brauchst in der äußeren for-Schleife einen Wahrheitswert, 
# um dir zu merken, ob du schon einen Teiler der äußeren Zahl gefunden hast.
# Nur wenn dieser Wahrheitswert am Ende False ist, ist die Zahl eine Primzahl.