# Gib die letzten 2 Stellen von x aus.
# (Dezimalsystem)
# Wenn die erste der beiden Stellen eine 0 ist, 
# ist auch nur die letzte Ziffer ok.
x = 436278402

# Dein Code... gibt hier z.B. 02 oder 2 aus.
