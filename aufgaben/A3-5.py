# Finde alle Paare a, b, c mit 
# 1 <= a, b, c <= 100, 
# sodass a^2 + b^2 = c^2.


# Für schnelle: 
# Gib davon nur die aus, für die c durch 10 teilbar ist.
# Für noch schnellere:
# Wenn du in der letzten Lösung Modulo benutzt hast, probier's ohne.