# Berechne für die Zahlen 41-45 jeweils ihr Quadrat und 
# betrachte die letzte Ziffer des Quadrats z.
# Dann gib' den jeweils passenden Buchstaben aus folgender Tabelle aus:
# z=0 oder z=2: I
# z=1: H
# z=3: E
# z=4: U
# z=5: A
# z=6: R
# z=7: T
# z=8: O
# z=9: R