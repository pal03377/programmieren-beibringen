# Finde die letzte Ziffer der Zahl, die sich ergibt, wenn man die Zahl 3 
# 5 mal mit 7 potenziert. Also die letzte Ziffer von ((3^7)^7)^7...
# Gib nur diese Ziffer aus.


# Für schnelle: Geht das auch noch gut, wenn man 10 oder 20 mal potenziert?
# Gibt es vielleicht Tricks, mit denen man da Probleme vermeiden kann?