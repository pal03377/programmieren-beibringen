# Schreibe ein möglichst kurzes Programm (wenige Zeichen), 
# das für eine beliebige anfängliche Variable x 
# den Wert 65 * x + 1 berechnet, aber ohne in deinem 
# Programm Multiplikation (*) zu benutzen:
# (Tipp 1 unten)

x = 14 # hier könnte irgend etwas stehen
# Dein Code...






































# Tipp 1: 
# 65 * x + 1 = 2^6 * x + x + 1
# = (2^5*x) + (2^5*x) + x + 1
# und damit kann man Sachen schon kürzer schreiben
# (Tipp 2 vvv)
























# Tipp 2: 
# 65 * x + 1 = 2^6 * x + x + 1
# = (2^5*x) + (2^5*x) + x + 1
# =    y    +    y    + x + 1 für y = 2^5*x
# und y kann man auch so ausrechnen
# y = 2^5 * x = (2^4 * x) + (2^4 * x)
#             =     z     +    z     für z = 2^4 * x
# wenn man immer so weiter macht, führt man einfach viele 
# Variablen ein, die man mit ? + ? immer verdoppelt